import React from "react";
import './style.scss';

const ButtonGoupPrime = ({step=0}) => {

    return(
        <div className={step > 5 ? "buttons-wrapper buttons-wrapper_active" : "buttons-wrapper"}>
            <button className={step > 7 ?
             "button button_active button-pink" :
             "button button-pink"}  >
                Авторизироваться
            </button>
            <button className={step > 7 ?
             "button button_active button-blue" :
             "button button-blue"}>
                Продолжить без авторизации
            </button>            
        </div>

    )
}
export default ButtonGoupPrime;