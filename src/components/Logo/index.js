import React, {useState} from 'react';
import logotip from './logo.png';
import './style.scss';


const Logo = ({step = 0}) => {

    return (
        <div className={step >= 1 ? 'logo logo_active' : 'logo'}>
            <img src={logotip} alt="I START U..." className='logo__img'/>
        </div>       
    )
}

export default Logo;