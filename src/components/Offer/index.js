import './style.scss';

const Offer = ({step = 0}) => {

    return(
        <div className={step > 3 ? "offer offer_active" : "offer"}>
            <p className={step > 4 ? "offer__text offer__text_active" :
             "offer__text"}>Уже <br/> зарегестрированы<br/> на START?
            </p>
            <p className={step > 4 ? "offer__text offer__text_active" :
             "offer__text"}>Войдите, а мы<br/> покажем на сколько<br/> хорошо мы вас знаем
            </p>
        </div>
    );
}

export default Offer;