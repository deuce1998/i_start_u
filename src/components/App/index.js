import React, {useState} from 'react';
import './reset.css';
import './App.scss';
import Logo from '../Logo';
import Offer from '../Offer';
import ButtonGoupPrime from '../ButtonGoupPrime';

function App() {
  const [scrollCount, setScrollCount] = useState(0);
  const [positionY, setPositionY] = useState(null);
  function wheelChange(e){
    e.deltaY > 0 ?
      scrollCount < 8 && setScrollCount(scrollCount + 1):
      scrollCount > 0 && setScrollCount(scrollCount - 1);
  }
  function handleTouchStart(e){
    setPositionY(e.changedTouches[0].clientY);
  }  
  function handleTouchMove(e){
    positionY > e.changedTouches[0].clientY ?
      scrollCount < 8 && setScrollCount(scrollCount + 1):
      scrollCount > 0 && setScrollCount(scrollCount - 1);
  }

  return (
    <div className="App">
      <section className='main' onWheel={wheelChange}
       onTouchStart={handleTouchStart} onTouchMove={handleTouchMove} >
        <div className="container main-container">
          {scrollCount >= 0 && <Logo step={scrollCount}/>}
          {scrollCount > 0 && <Offer step={scrollCount}/>}
          {scrollCount > 0 && <ButtonGoupPrime step={scrollCount}/>}             
        </div>       
      </section>
    </div>
  );
}

export default App;
